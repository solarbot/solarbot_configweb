const PART = 16000;
const MTU = 500;

var end = true;
var total = 0;
var fileParts = 0;

function printProgressBar(iteration, total) {
    const percent = ((iteration / total) * 100).toFixed(1);
    console.log('Progress: ', percent, '% Complete');
    document.getElementById('btnUpload').innerHTML = percent + '%';
}

function readUInt16BE(array, offset) {
    return (array[offset] << 8) | array[offset + 1];
}

function writeUInt16BE(array, value, offset) {
    array[offset] = (value & 0xff00) >> 8;    // Most significant byte
    array[offset + 1] = value & 0x00ff;       // Least significant byte
}

function writeUInt32BE(array, value, offset) {
    array[offset] = (value >>> 24) & 0xFF; // Most significant byte
    array[offset + 1] = (value >>> 16) & 0xFF;
    array[offset + 2] = (value >>> 8) & 0xFF;
    array[offset + 3] = value & 0xFF;          // Least significant byte
}

async function handleRx(event) {
    data = new Uint8Array(event.target.value.buffer);
    // if (data[0] === 0xAA) {
    //     console.log("Transfer mode:", data[1]);
    //     printProgressBar(0, total);
    //     if (data[1] === 1) {
    //         for (let x = 0; x < fileParts; x++) {
    //             await sendPart(x, updateData);
    //             printProgressBar(x + 1, total);
    //         }
    //     } else {
    //         await sendPart(0, updateData);
    //     }
    // }

    if (data[0] === 0xF1) {
        const nxt = readUInt16BE(data, 1);
        await sendPart(nxt, updateData);
        printProgressBar(nxt + 1, total);
    }

    if (data[0] === 0xF2) {
        console.log("Installing firmware");
        $("#btnUpload").html("UPLOAD");
    }

    if (data[0] === 0x0F) {
        const decoder = new TextDecoder('utf-8');
        const slicedUint8Array = data.slice(1);
        const utf8String = decoder.decode(slicedUint8Array);

        console.log("OTA result: ", utf8String);
        end = false;
    }
}

async function sendPart(position, data) {
    const start = position * PART;
    let end = (position + 1) * PART;
    if (data.byteLength < end) {
        end = data.byteLength;
    }
    const parts = Math.ceil((end - start) / MTU);
    for (let i = 0; i < parts; i++) {
        const toSend = new Uint8Array(MTU + 2);
        toSend[0] = 0xFB;
        toSend[1] = i;
        let slicedData = data.slice(position * PART + MTU * i, position * PART + MTU * (i + 1));
        toSend.set(new Uint8Array(slicedData), 2);
        await sendData(toSend);
    }
    if ((end - start) % MTU !== 0) {
        const rem = (end - start) % MTU;
        const toSend = new Uint8Array(rem + 2);
        toSend[0] = 0xFB;
        toSend[1] = parts;
        let slicedData = data.slice(position * PART + MTU * parts, position * PART + MTU * parts + rem);
        toSend.set(new Uint8Array(slicedData), 2);
        await sendData(toSend);
    }

    const update = new Uint8Array(5);
    update[0] = 0xFC;
    writeUInt16BE(update, end - start, 1);
    writeUInt16BE(update, position, 3);
    await sendData(update);
}

async function sendData(data) {
    await characteristic_ota_rx.writeValueWithoutResponse(data);
}

async function startOTA() {
    if (!bleDevice) {
        console.log("-----------Failed--------------");
        console.log('bleDevice with address ${bleAddress} could not be found.');
        return;
    }

    await sendData(Uint8Array.from([0xFD]));

    fileParts = Math.ceil(updateData.byteLength / PART);
    console.log('fileParts = ', fileParts);
    const fileSize = new Uint8Array(5);
    fileSize[0] = 0xFE;
    writeUInt32BE(fileSize, updateData.byteLength, 1);
    await sendData(fileSize);

    total = fileParts;
    const otaInfo = new Uint8Array(5);
    otaInfo[0] = 0xFF;
    writeUInt16BE(otaInfo, fileParts, 1);
    writeUInt16BE(otaInfo, MTU, 3);
    await sendData(otaInfo);

    await sendPart(0, updateData);

    while (end) {
        await new Promise((resolve) => setTimeout(resolve, 1000));
    }
    console.log("Waiting for disconnect... ");
    await disconnectedEvent;
    console.log("-----------Complete--------------");
}
