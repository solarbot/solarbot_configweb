//Define BLE Device Specs
const deviceNamePrefix = 'SCBot';
const SERVICE_UUID = '264489d0-e22f-49ba-ad96-085b7afd557c';
const USER_CHAR_UUID = '183baf1a-805f-49ba-811c-52120db8cb71';
const LOGIN_CHAR_UUID = 'f3528926-5dab-4383-876a-ddf597c73838';
const ADMIN_CHAR_UUID = 'ca80ca3a-4529-45a9-b6cf-61f35aee5d68';
const CLOCK_CHAR_UUID = 'd159bcd0-69de-4814-9c98-910ec25f1d1b';
const BATTERY_CHAR_UUID = '6e400002-b5a3-f393-e0a9-e50e24dcca9e';
const OTA_RX_CHAR_UUID = 'fb1e4002-54ae-4a28-9f74-dfccb248601d';
const OTA_TX_CHAR_UUID = 'fb1e4003-54ae-4a28-9f74-dfccb248601d';
//Global Variables to Handle Bluetooth
let bleServer;
let bleDevice;
let robotService;
let characteristic_user;
let characteristic_login;
let characteristic_admin;
let characteristic_clock;
let characteristic_battery;
let characteristic_ota_rx;
let characteristic_ota_tx;
let session_id;

let dataToSend = null;
let updateData = null;

var totalSize;
var remaining;
var amountToWrite;
var currentPosition;

var currentHardwareVersion = "N/A";
var softwareVersion = "N/A";
var latestCompatibleSoftware = "N/A";

const characteristicSize = 512;

$.urlParam = function (name) {
    let results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    return decodeURI(results[1]) || 0;
}

$(document).ready(function () {
    let valid = $.urlParam("valid");
    // update the text in the table
    if (valid == 0) {
        $("#logo").attr("src", "static/images/stop.png");
        $("#logo").attr("alt", "Invalid License");
        $("#valid").html("License is invalid");
        $("#valid_date").html("");
    } else {
        $("#logo").attr("src", "static/images/valid.png");
        $("#logo").attr("alt", "Valid License");
        $("#valid").html("License is valid");
        $("#valid_date").html("message");
    }

    $("#admin_page").hide();
    $("#user_page").hide();

    $("#btnAdmin").click(function () {
        $(".page_body").each(function () {
            $(this).hide();
        });

        handleLogin();
    });

    $("#btnConnect").click(function () {
        if (isWebBluetoothEnabled()) {
            if (bleServer && bleServer.connected) {
                btnDisconnectDevice();
            }
            else {
                btnConnectToDevice();
            }
        }
    });

    $("#btnSubmit").click(function () {
        handleSubmitAdmin();
    });

    $("#btnUpload").click(function () {
        let file = $("#license_key").prop('files')[0];

        var reader = new FileReader();
        reader.onload = function (e) {
            // binary data
            updateData = e.target.result;
            // debugger;
            return startOTA();
        };
        reader.onerror = function (e) {
            // error occurred
            console.log('Error : ' + e.type);
        };
        reader.readAsArrayBuffer(file);
    });
});

// Check if BLE is available in your Browser
function isWebBluetoothEnabled() {
    if (!navigator.bluetooth) {
        console.log("Web Bluetooth API is not available in this browser!");
        $('#connect-status').html("Web Bluetooth API is not available in this browser!");
        return false;
    }
    console.log('Web Bluetooth API supported in this browser.');
    return true;
}

// Connect to BLE Device and Enable Notifications
function btnConnectToDevice() {
    console.log('Initializing Bluetooth...');
    navigator.bluetooth.requestDevice({
        filters: [{ namePrefix: deviceNamePrefix }],
        optionalServices: [SERVICE_UUID]
    })
        .then(device => {
            bleDevice = device;
            console.log('Device Selected:', bleDevice.name);
            bleDevice.addEventListener('gattservicedisconnected', onDisconnected);

            return bleDevice.gatt.connect();
        })
        .then(server => {
            bleServer = server;
            console.log("Connected to GATT Server");
            $('#btnConnect').html("Disconnect");
            $('#connect-status').html('Connected to device ' + bleDevice.name);
            $('#connect-status').css("background", "#56c080");
            $("#user_page").show();

            return bleServer.getPrimaryService(SERVICE_UUID);
        })
        .then(service => {
            robotService = service;
            console.log("Service discovered:", service.uuid);
            return service.getCharacteristics();
        })
        .then(characteristics => {
            let promise_resolve = Promise.resolve();
            characteristics.forEach(characteristic => {
                console.log("characteristic.uuid = ", characteristic.uuid);

                switch (characteristic.uuid) {
                    case USER_CHAR_UUID:
                        characteristic_user = characteristic;
                        console.log("Found User charateristic");
                        promise_resolve.then(_ => {
                            return characteristic_user.readValue();
                        })
                            .then(read_doc => {
                                let decoded_doc = new TextDecoder().decode(read_doc);
                                let json_obj = JSON.parse(decoded_doc);
                                let t = new Date(json_obj.exp * 1000);
                                $('#expired_date').val(t.toISOString().slice(0, 10));
                                $('#customer').val(json_obj.cus);
                                $('#max_runtime').val(json_obj.lim);
                                $('#current_runtime').val(json_obj.run);
                                session_id = json_obj.ses;
                            });
                        break;
                    case LOGIN_CHAR_UUID:
                        characteristic_login = characteristic;
                        console.log("Found Login charateristic");
                        break;
                    case ADMIN_CHAR_UUID:
                        characteristic_admin = characteristic;
                        console.log("Found Admin charateristic");
                        break;
                    case CLOCK_CHAR_UUID:
                        characteristic_clock = characteristic;
                        characteristic_clock.addEventListener('characteristicvaluechanged', handleClockUpdate);
                        characteristic_clock.startNotifications();
                        console.log("Found Clock charateristic");
                        break;
                    case BATTERY_CHAR_UUID:
                        characteristic_battery = characteristic;
                        console.log("Found Battery charateristic");
                        break;
                    case OTA_TX_CHAR_UUID:
                        characteristic_ota_tx = characteristic;
                        // characteristic_ota.addEventListener('characteristicvaluechanged', SendBufferedData);
                        characteristic_ota_tx.addEventListener('characteristicvaluechanged', handleRx);
                        characteristic_ota_tx.startNotifications();
                        console.log("Found OTA_TX charateristic");
                        break;
                    case OTA_RX_CHAR_UUID:
                        characteristic_ota_rx = characteristic;
                        console.log("Found OTA_RX charateristic");
                        break;
                }
            });
        })
        .catch(error => {
            console.log('Error: ', error);
        })
}

function onDisconnected(event) {
    console.log("Robot disconnected:", event.target.device.name);
    $('#btnConnect').html("Connect");
    $('#connect-status').html("Robot disconnected. Click here to connect");
    $('#connect-status').css("background", "#555");
    $("#user_page").hide();
    $("#admin_page").hide();
    btnConnectToDevice();
}

function handleLogin() {
    if (bleServer && bleServer.connected) {
        let promise_resolve = Promise.resolve();
        promise_resolve.then(_ => {
            return (window.prompt("Please enter the OTP:"));
        })
            .then((password) => {
                if (password == null) {
                    return;
                }

                let doc = {
                    "ts": Math.floor(Date.now() / 1000),
                    "pw": password,
                    "ses": session_id
                };
                let encoded_doc = new TextEncoder().encode(JSON.stringify(doc));
                return characteristic_login.writeValueWithoutResponse(encoded_doc);
            })
            .then(() => {
                promise_resolve.then(_ => {
                    return characteristic_login.readValue();
                })
                    .then(read_doc => {
                        let decoded_doc = new TextDecoder().decode(read_doc);
                        let json_obj = JSON.parse(decoded_doc);

                        if (json_obj.auth == true) {
                            $('#admin_page').show();
                            $('#customer_admin').val(json_obj.cus);

                            let t = new Date(json_obj.del * 1000);
                            $('#delivery_date').val(t.toISOString().slice(0, 10));

                            $('#apn').val(json_obj.apn);
                            $('#gprs_user').val(json_obj.usr);
                            $('#gprs_password').val(json_obj.pwd);
                            $('#tb_server').val(json_obj.tbsrv);
                            $('#tb_client_id').val(json_obj.tbcid);
                            $('#tb_username').val(json_obj.tbusr);
                            $('#tb_password').val(json_obj.tbpwd);
                            $('#tb_port').val(json_obj.tbport);
                            $('#ntp_server').val(json_obj.ntpsrv);
                            $('#timezone').val(json_obj.tz);

                            t = new Date(json_obj.bld * 1000);
                            $('#build_date').val(t.toISOString().slice(0, 19));

                            t = new Date(json_obj.exp * 1000);
                            $('#end_date').val(t.toISOString().slice(0, 10));

                            $("#limit_runtime").val(json_obj.lim);
                            $("#ckb_activate").prop("checked", (json_obj.act === "true"));
                            if (json_obj.act === "true") {
                                $("#lbl_actiavte").text("Activated");
                            }
                            else {
                                $("#lbl_actiavte").text("Deactivated");
                            }

                        }
                        else {
                            $('#user_page').show();
                        };
                    })
            })
            .catch(error => {
                console.error("Error: ", error);
            })
    } else {
        console.error("Robot is not connected. Cannot write to characteristic.")
        window.alert("Robot is not connected. Cannot write to characteristic. \n Connect to Robot first!")
    }
}

function handleSubmitAdmin() {
    if (bleServer && bleServer.connected) {
        let promise_resolve = Promise.resolve();
        promise_resolve.then(_ => {
            let doc = {
                "ses": session_id,
                "cus": $("#customer_admin").val(),
                "del": Date.parse($("#delivery_date").val()) / 1000,
                "exp": Date.parse($("#end_date").val()) / 1000,
                "lim": $("#limit_runtime").val(),
                "act": ($("#ckb_activate").is(":checked")).toString(),
                "apn": $("#apn").val(),
                "usr": $("#gprs_user").val(),
                "pwd": $("#gprs_password").val(),
                "tbsrv": $("#tb_server").val(),
                "tbcid": $("#tb_client_id").val(),
                "tbusr": $("#tb_username").val(),
                "tbpwd": $("#tb_password").val(),
                "tbport": $("#tb_port").val(),
                "ntpsrv": $("#ntp_server").val(),
                "tz": $("#timezone").val(),
                "ts": Math.floor(Date.now() / 1000)
            };
            let encoded_doc = new TextEncoder().encode(JSON.stringify(doc));
            console.log(encoded_doc);
            return characteristic_admin.writeValueWithoutResponse(encoded_doc);
        })
            .then(() => {
                console.log("Write completed");
            })
            .catch(error => {
                console.error("Error: ", error);
            })
    } else {
        console.error("Robot is not connected. Cannot write to characteristic.")
        window.alert("Robot is not connected. Cannot write to characteristic. \n Connect to Robot first!")
    }
    let ts = Math.floor(Date.now() / 1000);
    $("#ts").val(ts.toString());
    $("#activate").val(($("#ckb_activate").is(":checked")).toString());
    $("#epoch_delivery_date").val(Date.parse($("#delivery_date").val()) / 1000);
    $("#epoch_end_date").val(Date.parse($("#end_date").val()) / 1000);
}

function btnDisconnectDevice() {
    console.log("Disconnect Device.");
    if (bleServer && bleServer.connected) {
        if (characteristic_clock) {
            characteristic_clock.stopNotifications()
                .then(() => {
                    console.log("Clock Notifications Stopped");
                    return bleServer.disconnect();
                })
                .then(() => {
                    console.log("Robot Disconnected");
                })
                .catch(error => {
                    console.log("An error occurred:", error);
                });
        } else {
            console.log("No characteristic found to disconnect.");
        }
    } else {
        // Throw an error if Bluetooth is not connected
        console.error("Robot is not connected.");
        window.alert("Robot is not connected.")
    }
}

function handleClockUpdate(event) {
    let value = new TextDecoder().decode(event.target.value);
    // console.log("value: ", value);
    let json_obj = JSON.parse(value);

    var clk = new Date(json_obj.clk * 1000);
    $("#internal_clock").val(clk.toUTCString());
}

function getDateTime() {
    let currentdate = new Date();
    let day = ("00" + currentdate.getDate()).slice(-2); // Convert day to string and slice
    let month = ("00" + (currentdate.getMonth() + 1)).slice(-2);
    let year = currentdate.getFullYear();
    let hours = ("00" + currentdate.getHours()).slice(-2);
    let minutes = ("00" + currentdate.getMinutes()).slice(-2);
    let seconds = ("00" + currentdate.getSeconds()).slice(-2);

    let datetime = day + "/" + month + "/" + year + " at " + hours + ":" + minutes + ":" + seconds;
    return datetime;
}